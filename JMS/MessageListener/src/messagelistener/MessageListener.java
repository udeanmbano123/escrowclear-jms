/** To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package messagelistener;

import com.sun.messaging.ConnectionConfiguration;
import com.sun.messaging.ConnectionFactory;
import com.sun.messaging.Queue;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 *
 * @author Administrator
 */
public class MessageListener extends JPanel {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws JMSException {
        ConnectionFactory connFactory = new ConnectionFactory();
        connFactory.setProperty(ConnectionConfiguration.imqAddressList,"10.10.10.76:7016");
Integer count=0;
        Queue myQueue = new Queue("interfaceCsdToTs");
      //HelloServiceBean my=new HelloServiceBean();
        Connection connection = connFactory.createConnection(); 
                com.sun.messaging.jms.Session session = (com.sun.messaging.jms.Session) connection.createSession(false, Session.AUTO_ACKNOWLEDGE); 
               
       	JFrame frame = new JFrame();
        frame.setSize(500,80);
    //JButton b1 = new JButton();
    //JButton b2 = new JButton();
    //  b1.setPreferredSize(new Dimension(40, 40));
     // b2.setPreferredSize(new Dimension(40, 40));   
    JLabel lb1=new JLabel("ESCROWSHARE JMS LISTENER RUNNING");

    /*b1.setVisible(true);
    b1.setText("START LISTENER");
    b2.setVisible(true);
    b2.setText("STOP LISTENER");
    */
    frame.setLayout(new GridLayout());
    frame.add(lb1);
    //frame.add(b1);
    //frame.add(b2);
    
    frame.setDefaultCloseOperation(0);
    frame.setVisible(true);
   frame.setResizable(false);
     
          
                           connection.start();
                      
                try{
                 // Create a QueueReceiver to receive messages
            MessageConsumer receiver=session.createConsumer(myQueue);

// Tell the Queue Connection you are ready to interact with the message service
            

             System.out.println("Checking the messages");

            for (;;)
            {
// Receive the next message
                TextMessage message = (TextMessage) receiver.receive();

// Print the message contents
                System.out.println(message.getText());
              SaveDatabaseIn(message.getText());
                connection.stop();
            }
        }
        catch (Exception exc)
        {
            exc.printStackTrace();
        }
        
                      
                         
                     
                 
}
        
        

    /**
     *
     * @param messages
     */
    public static void SaveDatabaseIn(String messages){
              	// Create a variable for the connection string.
			String connectionUrl = "jdbc:sqlserver://192.168.3.5:1433;databaseName=CDS;integratedSecurity=true;";
				// Declare the JDBC objects.
			java.sql.Connection conn = null;
			//String date =	d.toString() ;
	        	try {
	        		// Establish the connection.
	        		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
	            		conn = DriverManager.getConnection(connectionUrl);
	            
	            	     // the mysql insert statement
	            	      String query = "insert into tbl_Messaging ([Message_details],[Type],[Processed],[Sent])"
	            	        + " values (?, ?, ?, ?)";
	            
	            	      PreparedStatement preparedStmt = conn.prepareStatement(query);
	            	      preparedStmt.setString(1,messages);
	            	      preparedStmt.setString(2, "INBOUND");
                              preparedStmt.setString(3, "0");
                              preparedStmt.setString(4, "0");   	  
	            	      // execute the preparedstatement
	            	      preparedStmt.execute();
                       }
	      
               
			// Handle any errors that may have occurred.
			catch (Exception e) {
                            System.out.println(e.toString());
			}finally {
				//if (rs != null) try { rs.close(); } catch(Exception e) {}
		    		//if (stmt != null) try { stmt.close(); } catch(Exception e) {}
		    		if (conn != null) try { conn.close(); } catch(Exception e) {}
			}  
            }		
   
            		
   
    }
